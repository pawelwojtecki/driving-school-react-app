const now = new Date();

export default [
  {
    id: 0,
    title: 'B',
    start: new Date(2019, 3, 1, 11, 0, 0),
    end: new Date(2019, 3, 1, 13, 30, 0),
  },
  
  {
    id: 1,
    title: 'A / A2',
    start: new Date(2019, 3, 1, 14, 0, 0),
    end: new Date(2019, 3, 1, 16, 0, 0),
  },

  {
    id: 2,
    title: 'A1',
    start: new Date(2019, 3, 1, 16, 30, 0),
    end: new Date(2019, 3, 1, 17, 30, 0),
  },

  {
    id: 3,
    title: 'C+E',
    start: new Date(2019, 3, 2, 7, 30, 0),
    end: new Date(2019, 3, 2, 10, 30, 0),
  },

  {
    id: 4,
    title: 'A1',
    start: new Date(2019, 3, 2, 11, 0, 0),
    end: new Date(2019, 3, 2, 13, 0, 0),
  },

  // {
  //   id: 5,
  //   title: 'OBRONA PRACY MAGISTERSKIEJ',
  //   start: new Date(2019, 3, 4, 11, 0, 0),
  //   end: new Date(2019, 3, 4, 12, 0, 0),
  // },
  
  {
    id: 6,
    title: 'A2',
    start: new Date(2019, 3, 4, 14, 0, 0),
    end: new Date(2019, 3, 4, 16, 0, 0),
  },
  
  {
    id: 7,
    title: 'C',
    start: new Date(2019, 3, 5, 9, 30, 0),
    end: new Date(2019, 3, 5, 12, 0, 0),
  },
  
  {
    id: 8,
    title: 'A1',
    start: new Date(2019, 3, 5, 13, 0, 0),
    end: new Date(2019, 3, 5, 15, 0, 0),
  },
  
  {
    id: 9,
    title: 'A1',
    start: new Date(2019, 3, 5, 15, 15, 0),
    end: new Date(2019, 3, 5, 17, 0, 0),
  },
  
  {
    id: 10,
    title: 'C',
    start: new Date(2019, 3, 8, 9, 30, 0),
    end: new Date(2019, 3, 8, 12, 0, 0),
  },
  
  {
    id: 11,
    title: 'A1',
    start: new Date(2019, 3, 8, 13, 0, 0),
    end: new Date(2019, 3, 8, 15, 0, 0),
  },
  
  {
    id: 12,
    title: 'A1',
    start: new Date(2019, 3, 8, 15, 15, 0),
    end: new Date(2019, 3, 8, 17, 0, 0),
  },
  
  {
    id: 13,
    title: 'B',
    start: new Date(2019, 3, 9, 11, 0, 0),
    end: new Date(2019, 3, 9, 13, 30, 0),
  },
  
  {
    id: 14,
    title: 'A / A2',
    start: new Date(2019, 3, 9, 14, 0, 0),
    end: new Date(2019, 3, 9, 16, 0, 0),
  },

  {
    id: 15,
    title: 'A1',
    start: new Date(2019, 3, 9, 16, 30, 0),
    end: new Date(2019, 3, 9, 17, 30, 0),
  },

  {
    id: 16,
    title: 'A2',
    start: new Date(2019, 3, 10, 10, 0, 0),
    end: new Date(2019, 3, 10, 11, 30, 0),
  },
  
  {
    id: 17,
    title: 'A / A2',
    start: new Date(2019, 3, 10, 12, 30, 0),
    end: new Date(2019, 3, 10, 16, 0, 0),
  },

  {
    id: 18,
    title: 'A1',
    start: new Date(2019, 3, 10, 16, 30, 0),
    end: new Date(2019, 3, 10, 17, 30, 0),
  },

  {
    id: 19,
    title: 'A2',
    start: new Date(2019, 3, 11, 9, 0, 0),
    end: new Date(2019, 3, 11, 11, 30, 0),
  },
  
  {
    id: 20,
    title: 'B',
    start: new Date(2019, 3, 11, 12, 30, 0),
    end: new Date(2019, 3, 11, 14, 0, 0),
  },

  {
    id: 21,
    title: 'C+E',
    start: new Date(2019, 3, 11, 16, 30, 0),
    end: new Date(2019, 3, 11, 19, 30, 0),
  },

  {
    id: 22,
    title: 'A/A2',
    start: new Date(2019, 3, 12, 10, 0, 0),
    end: new Date(2019, 3, 12, 11, 30, 0),
  },
  
  {
    id: 23,
    title: 'B',
    start: new Date(2019, 3, 12, 12, 0, 0),
    end: new Date(2019, 3, 12, 14, 0, 0),
  },

  {
    id: 24,
    title: 'C+E',
    start: new Date(2019, 3, 12, 16, 30, 0),
    end: new Date(2019, 3, 12, 18, 30, 0),
  },
  
  {
    id: 25,
    title: 'OBRONA PRACY MAGISTERSKIEJ',
    start: new Date(new Date().setHours(new Date().getHours() - 1)),
    end: new Date(new Date().setHours(new Date().getHours() + 1)),
  },
  
]
