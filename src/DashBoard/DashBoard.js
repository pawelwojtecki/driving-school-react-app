import React, { Component } from 'react';
import './DashBoard.scss';

import 'bootstrap/dist/css/bootstrap.css';
import SideBarMenu from '../SideBarMenu/SideBarMenu';
import DrivingSchools from './DrivingSchools/DrivingSchools';
import Teachers from './Teachers/Teachers';

import { Route } from 'react-router-dom';
import DashBoardMain from './DashBoardMain';
import store from '../redux/store';
import InProgress from '../InProgress/InProgress';

class DashBoard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      school: true,
      teacher: false
    }
  }

  componentDidMount() {
    store.getState()
  }

  render() {
    return (
      <div>
        <div className='sidebar-column'>
          <SideBarMenu />
        </div>

        <div className='schools-column'>
          <Route exact path='/dashboard' component={DashBoardMain} />
          <Route exact path='/dashboard/driving-schools' component={DrivingSchools} />
          <Route exact path='/dashboard/teachers' component={Teachers} />
          <Route exact path='/dashboard/egzamin_in_progress' component={InProgress} />
          <Route exact path='/dashboard/kodeks_in_progress' component={InProgress} />
          <Route exact path='/dashboard/znaki_in_progress' component={InProgress} />
        </div>
      </div>
    );
  }

}

export default DashBoard;
