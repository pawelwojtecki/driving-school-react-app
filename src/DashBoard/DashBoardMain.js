import React, { Component } from 'react';
import './DashBoardMain.scss';

import okejka from '../statics/thumbs-up.png'

import 'bootstrap/dist/css/bootstrap.css';

class DashBoardMain extends Component {

  constructor(props) {
    super(props);

    this.state = {
      school: true,
      teacher: false
    }
  }


  render() {
    return (
    <div className='main-container'>
      <img src={okejka} className='image' />
      <div className='info'>DZIĘKUJEMY ZA DOŁĄCZENIE DO NASZEJ APLIKACJI. W TYM MIEJSCU BĘDĄ SIĘ POJAWIAĆ NEWSY NA TEMAT NOWYCH FUNKCJONALNOŚCI</div>
    </div>
    );
  }

}

export default DashBoardMain;

