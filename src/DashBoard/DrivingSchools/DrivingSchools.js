import 'bootstrap/dist/css/bootstrap.css';
import React, { Component } from 'react';
import './DrivingSchools.scss';
import School from './School';


class DrivingSchools extends Component {

  constructor(props) {
    super(props);

    this.state = {
      school: true,
      teacher: false
    }
  }

  schools = [
    {
      name: 'MOTO SZKOŁA WIGURY',
      categories: 'A, A1, A2, B, C, C+E',
      logo: 'W',
      address: 'Wigury 20, Łódź'
    },
    {
      name: 'AUTO JAZDA',
      categories: 'A, A1, A2, B, B1',
      logo: 'AJ',
      address: 'Warszawska 24, Łódź'
    },
    {
      name: 'KOWA L SKI MOTO SZKOŁA',
      categories: 'A, A1, A2, B',
      logo: 'K',
      address: 'Rawska 42, Skierniewice'
    },
    {
      name: 'ROGÓW OSK',
      categories: 'B, C+E, D',
      logo: 'R',
      address: 'Akacjowa 31, Rogów'
    },
    {
      name: 'LOK ŁÓDŹ',
      categories: 'A, A1, A2, B, C, C+E',
      logo: 'L',
      address: 'Brzezińska 532, Łódź'
    },
    {
      name: 'F1 SZKOŁA NAUKI JAZDY',
      categories: 'A, A1, A2, B, C, C+E',
      logo: 'F1',
      address: 'Pabianicka 64, Łódź'
    },
    {
      name: 'SPORT OSK',
      categories: 'A, A1, A2, B',
      logo: 'S',
      address: 'Lumumby 93, Łódź'
    },
    {
      name: 'ELKA KOBIECA NAUKA JAZDY',
      categories: 'A, A1, B',
      logo: 'E',
      address: 'Stefanowskiego 10, Łódź'
    },
  ]

  render() {
      return (
            <div className='background schools-container'>
                {this.schools.map((school, i) => <School key={i} school={school} />)}
            </div>
        );
  }

}

export default DrivingSchools;

