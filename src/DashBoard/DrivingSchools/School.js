import React, { Component } from 'react';
import './School.scss';

import 'bootstrap/dist/css/bootstrap.css';
import store from '../../redux/store';
import { joinToSchool } from '../../redux/actions';

class School extends Component {

    constructor(props) {
        super(props);

        this.state = {
            school: false,
            teacher: false,

        }
    }

    handleClick(event) {
        console.log(event)
        let school = event;
        store.dispatch(joinToSchool(school));
        this.setState({school: true});
        alert("Zapisano do szkoły: " + school.name + ". Potwierdzenie zapisu zostało wysłane na maila.");
        
    }

    joinToSchool(data) {
        console.log(data);
    }


    render() {
        return (
            <React.Fragment>
                <div className='back-card'>
                    <div className='school-card'>
                        <div className='school-info'>
                            <div className='school-name'>{this.props.school.name}</div>
                            <div className='school-address'>{this.props.school.address}</div>
                            <div className='school-categories'>Kategorie: {this.props.school.categories}</div>
                        </div>
                        <div className='school-logo'>{this.props.school.logo}</div>
                        <button onClick={() => this.handleClick(this.props.school)} className={this.state.school === true ? 'save-button active' : 'save-button'}>ZAPISZ SIĘ</button>
                    </div>
                </div>
            </React.Fragment>
        );
    }

}

export default School;
