import React, { Component } from 'react';
import './Teacher.scss';

import 'react-big-calendar/lib/css/react-big-calendar.css'
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import Selectable from '../../Calendar/Selectable';

class Teacher extends Component {

    constructor(props) {
        super(props);

        this.state = {
            school: true,
            teacher: false,
            localizer: BigCalendar.momentLocalizer(moment),
            showPopup: false
        }
    }

    togglePopup() {
        this.setState({
          showPopup: !this.state.showPopup
        });
      }

    render() {
        return (
            <React.Fragment>
                <div className='back-card'>
                    <div className='teacher-card'>
                        <div className='teacher-info'>
                            <div className='teacher-name'>{this.props.teacher.name}</div>
                            <div className='teacher-categories'>Kategorie: {this.props.teacher.categories}</div>
                        </div>
                        <div className='teacher-logo'>{this.props.teacher.logo}</div>
                        <button onClick={this.togglePopup.bind(this)} className='calendar-button'>KALENDARZ</button>
                    </div>
                </div>

            {
              this.state.showPopup ? 
              <Popup
                closePopup={this.togglePopup.bind(this)}
               />
                : null
            }
            
            </React.Fragment>
        );
    }

}

export default Teacher;



class Popup extends React.Component {
  
    constructor(props) {
      super(props);
  
      this.state = {
        school: true,
        teacher: false,
        localizer: BigCalendar.momentLocalizer(moment),
        showPopup: false
      }
    }
  
    render() {
      return (
        <div className='popup'>
          <div className='popup_inner'>
          <button onClick={this.props.closePopup} className='button center'>CLOSE</button>
            <Selectable localizer = {this.state.localizer} />
          </div>
        </div>
      );
    }
  }
  