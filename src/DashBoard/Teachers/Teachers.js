import React, { Component } from 'react';
import Teacher from './Teacher';
import './Teachers.scss';

class Teachers extends Component {

  constructor(props) {
    super(props);

    this.state = {
      school: true,
      teacher: false,
    }
  }

  teachers = [
    {
      name: 'ROMAN KRÓLAK',
      categories: 'A, A1, A2',
      logo: 'RK'
    },
    {
      name: 'STEFAN ROSŁY',
      categories: 'A, A1, A2, B, B1',
      logo: 'SR'
    },
    {
      name: 'KATARZYNA GUŹDŹ',
      categories: 'B',
      logo: 'KG'
    },
    {
      name: 'ANDRZEJ KUBA',
      categories: 'A, A1, B, C+E',
      logo: 'AK'
    },
    {
      name: 'SŁAWOMIR NOWAK',
      categories: 'A, A1, A2, B, C, C+E',
      logo: 'SN'
    },
    {
      name: 'WOJCIECH KOWALSKI',
      categories: 'B, C, C+E',
      logo: 'WK'
    },
    {
      name: 'ANNA WITEK',
      categories: 'A, A1, A2, B',
      logo: 'AW'
    },
    {
      name: 'BARBARA NATKO',
      categories: 'A, A1, B',
      logo: 'BK'
    },
  ]

  render() {
      return (
        <div>
           <div className='background teachers-container'>
               {this.teachers.map((teacher, i) => <Teacher key={i} teacher={teacher} />)}
           </div>
          </div>
      )}

}

export default Teachers;
