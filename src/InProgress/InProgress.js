import React, { Component } from 'react';
import './InProgress.scss';

import 'bootstrap/dist/css/bootstrap.css';
import budowa from '../statics/budowa.png'

class InProgress extends Component {

  constructor(props) {
    super(props);

    this.state = {
    }
  }

  componentDidMount() {
  }

  render() {
    return (
      <div className='inProgress-container'>
          <img src={budowa} className='image' />
          <div className='info'>STRONA W TRAKCIE PROJEKTOWANIA</div>
      </div>
    );
  }

}

export default InProgress;

