import React, { Component } from 'react';
import './LoginPage.scss';

import { Link } from 'react-router-dom';
import store from '../redux/store';

class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    componentDidMount() {
        this.checkCredentials();
    }

    checkCredentials() {
        let reduxState = store.getState();
        console.log(reduxState.users)

        let user = reduxState.users.filter(user => (user.email === this.state.email && user.password === this.state.password));
        if (user.length === 0 && this.state.email !== '' && this.state.password !== '') {
            this.setState({error: true});
            console.log(user);
            alert('Email lub hasło są niepoprawne')
        } else if (user.length !== 0) {
            console.log(user);
            this.props.history.replace('/dashboard');
        }
        return user;
    }

    handleSubmit(event) {

        let user = {
                    email: this.state.email,
                    password: this.state.password
        }

        if (user.email !== '' && user.password !== '') {
            localStorage.setItem('user', user.email);
            localStorage.setItem('password', user.password);
            this.checkCredentials();
        }

    }

    handleEmail(event) {
        this.setState({ email: event.target.value })
    }
    handlePassword(event) {
        this.setState({ password: event.target.value })
    }

    render() {
        
        return (
            <div className="login-page">
                <div className="login-background" />
                <div className="login-container">
                    <div className="login-box">

                        <div className="login-logo" />

                        <p className="login-title">LOGOWANIE</p>

                        <form onSubmit={this.handleSubmit}>
                            <div className="inputs">
                                <div className="input">
                                    <label htmlFor="email" className="inp">
                                        <input defaultValue={this.state.email} onChange={this.handleEmail} type="email" id="email" placeholder="&nbsp;" />
                                        <span className="label">Adres Email</span>
                                        <span className="border"></span>
                                    </label>
                                </div>

                                <div className="input">
                                    <label htmlFor="password" className="inp">
                                        <input defaultValue={this.state.password} onChange={this.handlePassword} type="password" id="password" placeholder="&nbsp;" />
                                        <span className="label">Hasło</span>
                                        <span className="border"></span>
                                    </label>
                                </div>
                            </div>
                            <button type="submit" className="button login-button">ZALOGUJ</button>
                        </form>

                        <div className="remember-me">
                            <input type="checkbox" /> <p>Zapamiętaj mnie</p> <p className="link">Zapomniałeś hasła?</p>
                        </div>
                        <div className="gray-info">Nie masz konta? <Link to='/register'><p className="link">Zarejestruj się!</p></Link></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoginPage;