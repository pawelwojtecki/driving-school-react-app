import React from 'react';
import './RegisterPage.scss';

import { Redirect, Link } from 'react-router-dom';
import LoginPage from '../LoginPage/LoginPage';
import store from '../redux/store';
import { registerUser } from '../redux/actions/index';

class RegisterPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
                name: '',
                surname: '',
                email: '',
                password: '',
                repeatPassword: ''
        };

        this.handleName = this.handleName.bind(this);
        this.handleSurname = this.handleSurname.bind(this);
        this.handleMail = this.handleMail.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleRepeatPassword = this.handleRepeatPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        let user = { 
                    name: this.state.name,
                    surname: this.state.surname,
                    email: this.state.email,
                    password: this.state.password 
                }
        store.dispatch(registerUser(user));
    }

    handleName(event) {
        this.setState({ name: event.target.value });
    }
    handleSurname(event) {
        this.setState({ surname: event.target.value });
    }
    handleMail(event) {
        this.setState({ email: event.target.value });
    }
    handlePassword(event) {
        this.setState({ password: event.target.value });
    }
    handleRepeatPassword(event) {
        this.setState({ repeatPassword: event.target.value })
    }

    render() {
        let hidden = '';
        (this.state.repeatPassword != this.state.password) ? hidden = '' : hidden = 'hidden';

        return (
            <div className="register-page">
                <div className="register-background" />
                <div className="register-container">
                    <div className="register-box">

                        <div className="register-logo" />

                        <p className="register-title">REJESTRACJA</p>

                        <form onSubmit={this.handleSubmit}>
                            <div className="inputs">
                                <div className="input">
                                    <label htmlFor="name" className="inp">
                                        <input defaultValue={this.state.name} onChange={this.handleName} type="text" id="name" placeholder="&nbsp;" />
                                        <span className="label">Imię</span>
                                        <span className="border"></span>
                                    </label>
                                </div>

                                <div className="input">
                                    <label htmlFor="surname" className="inp">
                                        <input defaultValue={this.state.surname} onChange={this.handleSurname} type="text" id="surname" placeholder="&nbsp;" />
                                        <span className="label">Nazwisko</span>
                                        <span className="border"></span>
                                    </label>
                                </div>

                                <div className="input">
                                    <label htmlFor="email" className="inp">
                                        <input defaultValue={this.state.email} onChange={this.handleMail} type="email" id="email" placeholder="&nbsp;" />
                                        <span className="label">Adres Email</span>
                                        <span className="border"></span>
                                    </label>
                                </div>

                                <div className="input">
                                    <label htmlFor="password" className="inp">
                                        <input defaultValue={this.state.password} onChange={this.handlePassword} type="password" id="password" placeholder="&nbsp;" />
                                        <span className="label">Hasło</span>
                                        <span className="border"></span>
                                    </label>
                                </div>

                                <div className="input">
                                    <label htmlFor="confirm_password" className="inp">
                                        <input defaultValue={this.state.repeatPassword} onChange={this.handleRepeatPassword} type="password" id="repeatPassword" placeholder="&nbsp;" />
                                        <span className="label">Powtórz hasło</span>
                                        <span className="border"></span>
                                    </label>
                                </div>

                                <span className={hidden}>Hasła róznią się od siebie.</span>
                            </div>

                            <Link to="/" onClick={this.handleSubmit}><button type="submit" className="button register-button">STWÓRZ KONTO</button></Link>
                            
                            {/* <button type="submit" className="button register-button">STWÓRZ KONTO</button> */}
                        </form>

                        <div className="agreement">
                            <p>Tworząc konto akceptujesz</p> <p className="link">regulamin</p>
                        </div>
                        <Link to="/"><div className="gray-info">Masz już konto? <p className="link">Zaloguj się!</p></div></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default RegisterPage;