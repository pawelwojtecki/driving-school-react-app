import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';

class SafeLink extends Component {
    onClick (event) {
        if (this.props.to === this.props.history.loaction.pathname) {
            event.preventDeafult();
        }

        if(this.props.onClick) {
            this.props.onClick();
        }
    }

    render() {
        const { children, onClick, ...other } = this.props;
        return <NavLink activeClassName='active' onClick={this.onClick.bind(this)} exact to {...other}>{ children }</NavLink>
    }
}

export default withRouter(SafeLink);
