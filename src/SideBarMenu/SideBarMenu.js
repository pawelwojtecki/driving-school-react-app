import React, { Component } from 'react';
import SafeLink from '../Services/SafeLink';
import './SideBarMenu.scss';


class SideBarMenu extends Component {
    render() {
        return (
            <div className="side-bar-container">
                <div className="side-bar-logo">
                    <div className="side-logo" />
                    <p>AutoGraph</p>
                </div>
                <div className="side-bar-links">

                    <SafeLink to='/dashboard' className='safe-link'><div className="side-bar-link">STRONA GŁÓWNA</div></SafeLink>

                    <SafeLink to='/dashboard/driving-schools' className='safe-link'><div className="side-bar-link">ZAPISY DO SZKÓŁ JAZDY</div></SafeLink>

                    <SafeLink to='/dashboard/teachers' className='safe-link'><div className="side-bar-link">ZAPISY DO INSTRUKTORÓW</div></SafeLink>

                    <SafeLink to='/dashboard/egzamin_in_progress' className='safe-link'><div className="side-bar-link">ZAPISY NA EGZAMINY</div></SafeLink>

                    <SafeLink to='/dashboard/kodeks_in_progress' className='safe-link'><div className="side-bar-link">KODEKS DROGOWY</div></SafeLink>

                    <SafeLink to='/dashboard/znaki_in_progress' className='safe-link'><div className="side-bar-link">ZNAKI DROGOWE</div></SafeLink>

                    <SafeLink to='/' className='safe-link'><div className="side-bar-link">WYLOGUJ</div></SafeLink>
                </div>
            </div >
        );
    }
}

export default SideBarMenu;
