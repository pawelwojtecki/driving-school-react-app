import { ADD_ARTICLE, REGISTER_USER, JOIN_TO_SCHOOL } from "../constants/action-types";

export function addArticle(payload) {
    return { type: ADD_ARTICLE, payload }
};

export function registerUser(payload) {
    return { type: REGISTER_USER, payload }
}

export function joinToSchool(payload) {
    return { type: JOIN_TO_SCHOOL, payload }
}
