import { ADD_ARTICLE, REGISTER_USER, JOIN_TO_SCHOOL, JOIN_TO_TEACHER } from '../constants/action-types'

const initialState = {
    articles: [],
    users: [
        {
            name: 'Robert',
            surename: 'Renwort',
            email: 'asd@a.com',
            password: 'password',
            school: '',
            teacher: '',
            category: '',
            time: ''
        },
        {
            name: 'Andrew',
            surename: 'Tworden',
            email: 'andrew@a.com',
            password: 'haslo',
            school: '',
            teacher: '',
            category: '',
            time: ''
        },
        {
            name: 'Paweł',
            surename: 'Wojtecki',
            email: 'pawelwojtecki@gmail.com',
            password: 'magister',
        }
    ],
    school: [],
    teacher: []
};

function rootReducer (state = initialState, action) {
    switch (action.type) {
        case ADD_ARTICLE:
            return { ...state, articles: state.articles.concat(action.payload) }
        case REGISTER_USER:
            return { ...state, users: state.users.concat(action.payload) }           
        case JOIN_TO_SCHOOL:
            return { ...state, school: state.school.concat(action.payload) }
        case JOIN_TO_TEACHER:
            return { ...state, teacher: state.teacher.concat(action.payload) }
    }

    // if (action.type === ADD_ARTICLE) {
    //     return Object.assign({}, state, {
    //         articles: state.articles.concat(action.payload)
    //     });
    // }
    return state;
}

export default rootReducer;
